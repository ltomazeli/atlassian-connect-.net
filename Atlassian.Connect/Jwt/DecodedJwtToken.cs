using System;
using System.Collections.Generic;
using System.Web;
using Atlassian.Connect.Internal;
using JWT;

namespace Atlassian.Connect.Jwt
{
    public class DecodedJwtToken
    {
        private static readonly QueryStringHasher Hasher = new QueryStringHasher();

        public DecodedJwtToken(string sharedSecret)
        {
            SharedSecret = sharedSecret;
            Claims = new Dictionary<string, object>();
        }

        public string SharedSecret { get; set; }
        public IDictionary<string, object> Claims { get; set; }

        public EncodedJwtToken Encode()
        {
            return new EncodedJwtToken(SharedSecret, JsonWebToken.Encode(Claims, SharedSecret, JwtHashAlgorithm.HS256));
        }

        public void ValidateToken(HttpRequestBase request)
        {
            ValidateTokenHasNotExpired();
            ValidateQueryStringHash(request);
        }

        private void ValidateTokenHasNotExpired()
        {
            var expiresAt = Convert.ToInt64(Claims["exp"]);

            var now = DateTime.UtcNow.AsUnixTimestampSeconds();

            if (expiresAt < now)
                throw new Exception("JWT Token Expired");
        }

        private void ValidateQueryStringHash(HttpRequestBase request)
        {
            var path = VirtualPathUtility.MakeRelative("~", request.Url.AbsolutePath);
            if (!path.StartsWith("/"))
                path = "/" + path;

            var qsh = Hasher.CalculateHash(request.HttpMethod, path, request.Url.Query.TrimStart('?'));
            var requestQsh = Claims["qsh"] as string;

            if (qsh != requestQsh)
                throw new Exception("QSH Does not match.");
        }
    }
}