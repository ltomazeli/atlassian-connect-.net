using System;
using System.Web.Mvc;

namespace Atlassian.Connect.Jwt
{
    public class JwtAuthentication :
        ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                var requestToken = filterContext.HttpContext.Request.GetJwtToken();
                var clientKey = filterContext.HttpContext.Request.GetClientKey();

                if (String.IsNullOrEmpty(requestToken))
                {
                    throw new Exception("Authentication failed, missing JWT token");
                }

                var skp = SecretKeyProviderFactory.GetSecretKeyProvider();
                var secretKey = skp.GetSecretKey(clientKey);

                var token = new EncodedJwtToken(secretKey, requestToken).Decode();
                token.ValidateToken(filterContext.HttpContext.Request);
            }
            catch (Exception)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }
    }
}