﻿using System.Collections.Generic;
using JWT;

namespace Atlassian.Connect.Jwt
{
    public class EncodedJwtToken
    {
        public EncodedJwtToken(string sharedSecret, string jwtToken)
        {
            SharedSecret = sharedSecret;
            JwtTokenString = jwtToken;
        }

        public string SharedSecret { get; set; }
        public string JwtTokenString { get; set; }

        public DecodedJwtToken Decode()
        {
            return new DecodedJwtToken(SharedSecret)
            {
                Claims = JsonWebToken.DecodeToObject(JwtTokenString, SharedSecret) as IDictionary<string, object>
            };
        }
    }
}
