﻿using System.Web;
using Atlassian.Connect.Entities;
using Newtonsoft.Json;

namespace Atlassian.Connect
{
    public static class SecretKeyPersister
    {
        public static void SaveSecretKey(HttpRequestBase request)
        {
            using (var dbContext = new InstancesContext())
            {
                var bodyText = new System.IO.StreamReader(request.InputStream).ReadToEnd();

                var instanceData = JsonConvert.DeserializeObject<InstalledInstance>(bodyText);

                dbContext.InstalledInstances.Add(instanceData);
                dbContext.SaveChanges();
            }
        }
    }
}