﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Atlassian.Connect.Jwt;

namespace Atlassian.Connect.Internal
{
    class JwtInHttpClientMessageHandler : 
        DelegatingHandler
    {
        private readonly HttpRequestBase _request;
        private readonly string _sharedSecret;
        private readonly string _addOnKey;
        private static readonly QueryStringHasher Hasher = new QueryStringHasher();
        private readonly string _clientKey;

        public JwtInHttpClientMessageHandler(HttpRequestBase request, string sharedSecret, string addOnKey)
        {
            _request = request;
            _sharedSecret = sharedSecret;
            _addOnKey = addOnKey;
            _clientKey = request.GetClientKey();
            InnerHandler = new HttpClientHandler();
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var path = request.RequestUri.AbsolutePath;
            if (!String.IsNullOrEmpty(_request.QueryString["cp"]))
            {
                path = path.Replace(_request.QueryString["cp"], "");
            }
            if (!path.StartsWith("/"))
                path = "/" + path;

            var qsh = Hasher.CalculateHash(request.Method.ToString(), path,
                request.RequestUri.Query.TrimStart('?'));
        
            var tokenBuilder = new DecodedJwtToken(_sharedSecret);
            tokenBuilder.Claims.Add("qsh", qsh);
            tokenBuilder.Claims.Add("iat", DateTime.UtcNow.AsUnixTimestampSeconds());
            tokenBuilder.Claims.Add("exp", DateTime.UtcNow.AddMinutes(5).AsUnixTimestampSeconds());
            tokenBuilder.Claims.Add("iss", _addOnKey);
            tokenBuilder.Claims.Add("sub", _clientKey);

            var jwt = tokenBuilder.Encode().JwtTokenString;

            request.Headers.Add("Authorization", "JWT " + jwt);

            return base.SendAsync(request, cancellationToken);
        }
    }
}