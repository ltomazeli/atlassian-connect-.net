﻿using System;
using Atlassian.Connect.Entities;

namespace Atlassian.Connect
{
    public static class SecretKeyProviderFactory
    {
        static SecretKeyProviderFactory()
        {
            Factory = () => new SecretKeyProviderImpl();
        }

        public static ISecretKeyProvider GetSecretKeyProvider()
        {
            return Factory();
        }

        public static Func<ISecretKeyProvider> Factory { get; set; } 
    }
}