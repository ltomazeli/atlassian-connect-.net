using Atlassian.Connect.Implementation;
using Newtonsoft.Json;

namespace Atlassian.Connect
{
    public class ConnectDescriptorVendor :
        DynamicBaseImplementation
    {
        [JsonProperty]
        public string name { get; set; }
        [JsonProperty]
        public string url { get; set; }
    }
}