using System.Web;

namespace Atlassian.Connect
{
    public static class ConnectDescriptorExtensions
    {
        public static void SetBaseUrlFromRequest(this ConnectDescriptor descriptor, HttpRequestBase request)
        {
            descriptor.baseUrl = request.Url.Scheme + "://" + request.Url.Authority +
                                 request.ApplicationPath.TrimEnd('/') + "/";
        }
    }
}