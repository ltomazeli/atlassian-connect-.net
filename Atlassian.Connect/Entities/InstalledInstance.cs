﻿namespace Atlassian.Connect.Entities
{
    public class InstalledInstance
    {
        public virtual int Id { get; set; }

        public virtual string BaseUrl { get; set; }
        public virtual string ClientKey { get; set; }
        public virtual string SharedSecret { get; set; }
    }
}
