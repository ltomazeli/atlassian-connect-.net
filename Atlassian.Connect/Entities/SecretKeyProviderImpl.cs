﻿using System.Linq;

namespace Atlassian.Connect.Entities
{
    public class SecretKeyProviderImpl :
        ISecretKeyProvider
    {
        public string GetSecretKey(string clientKey)
        {
            using (var dbContext = new InstancesContext())
            {
                return dbContext.InstalledInstances
                    .Where(x => x.ClientKey == clientKey)
                    .OrderByDescending(x => x.Id)
                    .Select(x => x.SharedSecret)
                    .FirstOrDefault();
            }
        }
    }
}