﻿using System.Data.Entity;

namespace Atlassian.Connect.Entities
{
    public class InstancesContext : DbContext
    {
        public InstancesContext()
            : base("SharedSecretsConnectionString")
        {
        }

        public DbSet<InstalledInstance> InstalledInstances { get; set; }
    }
}