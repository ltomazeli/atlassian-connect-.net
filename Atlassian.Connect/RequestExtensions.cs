﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web;
using Atlassian.Connect.Entities;
using Atlassian.Connect.Internal;
using JWT;

namespace Atlassian.Connect
{
    public static class RequestExtensions
    {
        public static HttpClient CreateConnectHttpClient(this HttpRequestBase request, string addOnKey)
        {
            var clientKey = request.GetClientKey();
            var client = new HttpClient(new JwtInHttpClientMessageHandler(request, new SecretKeyProviderImpl().GetSecretKey(clientKey), addOnKey));

            var baseUrl = request.QueryString["xdm_e"] + request.QueryString["cp"];
            if (!string.IsNullOrEmpty(baseUrl))
            {
                // normalize if there's a slash
                baseUrl = baseUrl.TrimEnd('/') + "/";
                client.BaseAddress = new Uri(baseUrl);
            }
            return client;
        }

        public static string GetClientKey(this HttpRequestBase request)
        {
            var claims = JsonWebToken.DecodeToObject(request.GetJwtToken(), string.Empty, false) as IDictionary<string, object>;
            return claims["iss"] as string;
        }

        public static string GetJwtToken(this HttpRequestBase request)
        {
            var requestToken = request.QueryString["jwt"];
            if (String.IsNullOrEmpty(requestToken))
            {
                requestToken = request.Headers["Authorization"];
                if (!String.IsNullOrEmpty(requestToken))
                {
                    requestToken = requestToken.Replace("JWT ", "");
                }
            }
            return requestToken;
        }
    }
}