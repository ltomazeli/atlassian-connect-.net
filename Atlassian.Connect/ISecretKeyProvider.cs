﻿namespace Atlassian.Connect
{
    public interface ISecretKeyProvider
    {
        string GetSecretKey(string clientKey);
    }
}