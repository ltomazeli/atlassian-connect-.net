using System.Collections.Generic;
using Newtonsoft.Json;
using Atlassian.Connect.Implementation;

namespace Atlassian.Connect
{
    public class ConnectDescriptor :
        DynamicBaseImplementation
    {
        public ConnectDescriptor()
        {
            scopes = new List<string>();
            vendor = new ConnectDescriptorVendor();
        }

        [JsonProperty]
        public string name { get; set; }
        [JsonProperty]
        public string description { get; set; }
        [JsonProperty]
        public string key { get; set; }
        [JsonProperty]
        public string baseUrl { get; set; }
        [JsonProperty]
        public ConnectDescriptorVendor vendor { get; set; }
        [JsonProperty]
        public dynamic authentication { get; set; }
        [JsonProperty]
        public dynamic lifecycle { get; set; }
        [JsonProperty]
        public dynamic modules { get; set; }
        [JsonProperty]
        public IList<string> scopes { get; set; }
    }
}