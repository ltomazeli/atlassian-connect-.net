// include Fake
#r @"packages/FAKE/tools/FakeLib.dll"
open Fake
open Fake.AssemblyInfoFile
open Fake.Git

RestorePackages()

// Properties
let version = "1.0.2"

let baseBuildDir = "./build"
let buildDir = baseBuildDir @@ "/build/"
let deployDir = baseBuildDir @@ "/deploy/"

// Default target
Target "Default" (fun _ ->
    trace "Starting build..."
)

// Other targets
Target "Clean" (fun _ ->
    CleanDir baseBuildDir
    CreateDir buildDir
    CreateDir deployDir
)

Target "BuildAssembly" (fun _ ->

    let commitHash = Information.getCurrentSHA1(".")

    CreateCSharpAssemblyInfo "./Atlassian.Connect/Properties/AssemblyInfo.cs"
        [Attribute.Title "Atlassian Connect .NET"
         Attribute.Description "Atlassian Connect Framework for .NET"
         Attribute.Copyright "Copyright 2014-2015 Atlassian Pty Ltd."
         Attribute.Company "Atlassian"
         Attribute.Product "Atlassian Connect"
         Attribute.Guid "1debf53e-1a21-462a-8eda-9646f5e96c45"
         Attribute.ComVisible false
         Attribute.Metadata("githash", commitHash)
         Attribute.Version version
         Attribute.FileVersion version]

    !! "Atlassian.Connect/*.csproj"
      |> MSBuildRelease buildDir "Build"
      |> Log "AppBuild-Output: "
)

Target "Package" (fun _ ->
    let net45Dir = deployDir @@ "lib/net45/"

    CopyFiles net45Dir !! (buildDir @@ "Atlassian.Connect.*")
    CopyFiles deployDir ["LICENSE.txt"; "README.md"; "CHANGELOG.md"]

    NuGet (fun p -> 
        {p with
            OutputPath = baseBuildDir
            WorkingDir = deployDir
            Files = [ ("**/*.*", None, None) ]
            Version = version
            Dependencies =
                ["JWT", GetPackageVersion "./packages/" "JWT"
                 "EntityFramework", GetPackageVersion "./packages/" "EntityFramework"
                 "Microsoft.AspNet.Mvc", GetPackageVersion "./packages/" "Microsoft.AspNet.Mvc"
                 "Microsoft.AspNet.Razor", GetPackageVersion "./packages/" "Microsoft.AspNet.Razor"
                 "Microsoft.AspNet.WebApi.Client", GetPackageVersion "./packages/" "Microsoft.AspNet.WebApi.Client"
                 "Microsoft.AspNet.WebApi.Core", GetPackageVersion "./packages/" "Microsoft.AspNet.WebApi.Core"
                 "Microsoft.AspNet.WebPages", GetPackageVersion "./packages/" "Microsoft.AspNet.WebPages"
                 "Microsoft.Web.Infrastructure", GetPackageVersion "./packages/" "Microsoft.Web.Infrastructure"
                 "Newtonsoft.Json", GetPackageVersion "./packages/" "Newtonsoft.Json"
                ]
            AccessKey = getBuildParamOrDefault "nugetkey" ""
            Publish = hasBuildParam "nugetkey" }) "connect.nuspec"
)

// Dependencies
"Clean"
  ==> "BuildAssembly"
  ==> "Package"
  ==> "Default"

// start build
RunTargetOrDefault "Default"